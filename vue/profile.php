<?php

if (!empty($_SESSION['userid'])) {
    $user = getData('user', 'id', $_SESSION['userid']);

    if (is_a($user, 'stdClass')){
        $output = '
        <h2>Profil</h2>
        <table>
            <thead>
                <tr>
                    <th>Intitulé</th>
                    <th>Valeur</th>
                </tr>
        </thead>
        <tbody>
        ';
        if ($user->image != null) {
            $output .= '<tr><td>Photo de Profil</td><td><img src="' . $user->image . '" alt="photo de profil"></td></tr>';
        }
        foreach ($user as $key => $value) {
            if ($key == 'password' || $key == 'image') {
                continue;
            } elseif ($key == 'created' || $key == 'lastlogin') {
                if ($key == 'lastlogin') {
                    $key = 'derniere connexion';
                } else {
                    $key = 'inscription';
                }
                $value = transform_date($value);
            } elseif ($key == 'admin') {
                if ($value == 1) {
                    $value = 'oui';
                } else {
                    $value = 'non';
                }
            }
            $output .= '<tr><td>' . ucfirst($key) . '</td><td>' . $value . '</td></tr>';
        }

        $output .= '
        </tbody>
    </table>
    <hr>
    <h3> Gestion profile </h3>
    <div class="gest_profil">
        <a href="index.php?vue=vue/update"> Update </a>
        <a href="app/export.php"> Export </a>   
    </div>
    ';

    echo $output;
    } else {
        setAlert('Erreur de DB', 'index.php?vue=vue/login');
    }
} else {
    setAlert('Aucun Utilisateur connecté!', 'index.php?vue=vue/login', 'info');
}
