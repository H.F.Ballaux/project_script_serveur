<?php

$cours = getList('course');

$output = '
<h2>Liste des Cours</h2>
<table>
    <thead>
        <tr>
        <th>Intitulé</th>
        <th>code</th>
        </tr>
    </thead>
    <tbody>
';

foreach ($cours as $cour) {
    $output .= '
        <tr>
            <td>' . $cour['name'] . '</td>
            <td>' . $cour['code'] . '</td>
        </tr>
    ';
}

$output .= '
    </tbody>
</table>
';

echo $output;
