<?php

if (!empty($_SESSION['userid'])) {
    $user = getData('user', 'id', $_SESSION['userid']);
    $output = '
    <h1>Mise a jour du profil de ' . ucfirst($user->username) . '</h1>
    
    <form action="index.php?vue=app/update" method="post">
        <input type="hidden" value="' . $user->id . '" name="id">
        <fieldset>
            <legend>Mot de passe </legend>
            <label for="password_old">Ancien
                <input type="password" id="password_old" name="password_old" placeholder="Ancien mot de passe">
            </label><br>
            <label for="password">Nouveau
                <input type="password" id="password" name="password" placeholder="Nouveau mot de passe">
            </label><br>
        </fieldset>
        <fieldset>
            <legend>Email </legend>
            <label for="email">Email 
                <input type="email" id="email" name="email" value="' . $user->email . '">
            </label><br>
        </fieldset>
        <input type="submit" value="Valider">
    </form>
    ';

    echo $output;
} else {
    setAlert('Vous n\'êtes pas des nôtres', 'index.php?vue=vue/login', 'info');
}
