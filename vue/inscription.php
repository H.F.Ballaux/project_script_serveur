<?php
$output = '
<h2>Inscription</h2>

<form action="index.php?vue=app/inscription" method="post" enctype="multipart/form-data">
    <label for="username">
        <input type="text" id="username" name="username" placeholder=" - Login - ">
    </label><br>
    <label for="pwd">
        <input type="password" id="pwd" name="pwd" placeholder=" - Password - ">
    </label><br>
    <label for="email">
        <input type="email" id="email" name="email" placeholder=" - Email - ">
    </label><br>
    <label class="photo" for="photo">Photo de profil</label><br>
    <input type="file" id="photo" name="photo" accept="image/jpeg,image/png, image/jpg">
    <br>
    <input type="submit" value="S\'inscrire">
</form>
';

echo $output;
