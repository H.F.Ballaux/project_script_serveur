<?php


$output = '
<nav>
    <a href="index.php">Accueil</a>
    <a href="index.php?vue=vue/liste_cours">Liste des cours</a>
';
if (!empty($_SESSION['userid'])) {
    $output .= '
        <a href="index.php?vue=vue/profile">Profil</a>
        <a href="index.php?vue=vue/logout">Logout</a>
    ';
    if (getData('user', 'id', $_SESSION['userid'])->admin) {
        $output .= '<a href="index.php?vue=vue/admin">Admin</a>';
    }
} else {
    $output .= '<a href="index.php?vue=vue/login">Login</a>';
}
$output .= '
</nav>
';

echo $output;
