<?php

$msg = ' -- Admin -- <br>';
$url = 'index.php?vue=vue/';
$output = '';

if (!empty($_SESSION['userid'])) {
    if (getData('user', 'id', $_SESSION['userid'])->admin) {
        $users = getList('user');
        if (sizeof($users) > 0) {
            $output .= '
            <h2>Liste des utilisateurs</h2>
            <table>
                <thead>
                <tr>
            ';
            foreach ($users[0] as $key => $value) {
                if (in_array($key, ['password', 'image'])) {
                    continue;
                } else {
                    $output .= '
                    <th>' . $key . '</th>
                    ';
                }
            }
            $output .= '<th>Set admin</th>
                    </tr>
                </thead>
                <tbody>
            ';
            foreach ($users as $user) {
                $output .= '<tr>';
                $btn_admin = '';
                foreach ($user as $key => $value) {
                    if (in_array($key, ['password', 'image'])) {
                        continue;
                    } else {
                        if ($key == 'created' || $key == 'lastlogin') {
                            try {
                                $value = transform_date($value);
                            } catch (Exception $e) {
                                echo 'error : ' . $e->getMessage();
                            }
                        } elseif ($key == 'admin') {
                            if ($value == 1) {
                                $value = 'Oui';
                                $btn_admin = 'unset';
                            } else {
                                $value = 'Non';
                                $btn_admin = 'set';
                            }
                        }
                        $output .= '
                        <td>' . $value . '</td>
                        ';
                    }
                }
                $output .= '<td><form action="index.php?vue=app/'.$btn_admin.'_admin" method="post"><input type="hidden" name="id" value="'.$user['id'].'"><input type="submit" value="'.$btn_admin.'"></form></td></tr>';
            }
            $output .= '
                </tbody>
            </table>
            ';
        } else {
            setAlert($msg . 'Il n\'existe personne dans la DB', $url . 'logout');
        }
    } else {
        setAlert($msg . 'Vous n\'avez pas accès a cette zone!', $url . 'profile', 'warning');
    }
} else {
    setAlert($msg . 'Vous n\'êtes pas encore des nôtres', $url . 'login', 'info');
}
echo $output;
