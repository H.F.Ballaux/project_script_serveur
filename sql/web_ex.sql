-- Creation de la DB projectScriptServer

-- drop database if exists projectScriptServer;
-- create database if not exists projectScriptServer;
-- use projectScriptServer;


-- Structure de la table `user`

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user`
(
    `id`        int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `username`  varchar(255)     NOT NULL,
    `password`  varchar(255)     NOT NULL,
    `email`     varchar(255)     NOT NULL,
    `created`   datetime         NOT NULL,
    `lastlogin` datetime         NOT NULL,
    `admin`     tinyint(1)   DEFAULT 0,
    `image`     varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`),
    UNIQUE KEY `username` (`username`)
) ENGINE = InnoDB;

-- triggers pour la table user --
-- -- permet d'attribuer le statut admin au premier inscrit
delimiter |
create trigger bi_user before insert
    on user for each row
        begin
            declare cpt_user int default 0;
            select count(*) into cpt_user from user;
            if cpt_user = 0 then
                set NEW.admin = 1;
            end if;
        end |
delimiter ;

-- -- verifie le nombre d'admin, empeche l'update si ce n'est pas le cas
delimiter |
create trigger bu_user before update
    on user for each row
        begin
            declare cpt_user int default 0;
            select count(*) into cpt_user from user where admin = 1;
            if NEW.admin != OLD.admin and NEW.admin = 0 then
                if cpt_user <= 1 then
                    signal sqlstate '99999' set message_text = 'Operation impossible';
                end if;
            end if ;
        end |
delimiter ;


-- Structure de la table `course`

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course`
(
    `id`   bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(255)        NOT NULL,
    `code` varchar(4)          NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `code` (`code`)
) ENGINE = InnoDB;


-- Déchargement des données de la table `course`

INSERT INTO `course` (`id`, `name`, `code`)
VALUES (1, 'Base des réseaux', '1351'),
       (2, 'Environnement et technologies du web', '1352'),
       (3, 'SGBD (Système de gestion de bases de données)', '1353'),
       (4, 'Création de sites web statiques', '1354'),
       (5, 'Approche Design', '1355'),
       (6, 'CMS - niveau 1', '1356'),
       (7, 'Initiation à la programmation', '1357'),
       (8, 'Activités professionnelles de formation', '1358'),
       (9, 'Scripts clients', '1359'),
       (10, 'Scripts serveurs', '1360'),
       (11, 'Framework et POO côté Serveur', '1361'),
       (12, 'Projet Web dynamique', '1362'),
       (13, 'Veille technologique', '1363'),
       (14, 'Epreuve intégrée', '1364'),
       (15, 'Anglais UE1', '1783'),
       (16, 'Anglais UE2', '1784'),
       (17, 'Initiation aux bases de données', '1440'),
       (18, 'Principes algorithmiques et programmation', '1442'),
       (19, 'Programmation orientée objet', '1443'),
       (20, 'Web : principes de base', '1444'),
       (21, 'Techniques de gestion de projet', '1448'),
       (22, 'Principes d’analyse informatique', '1449'),
       (23, 'Eléments de statistique', '1755'),
       (24, 'Structure des ordinateurs', '1808'),
       (25, 'Gestion et exploitation de bases de données', '1811'),
       (26, 'Mathématiques appliquées à l’informatique', '1807'),
       (27, 'Bases des réseaux', '1323'),
       (28, 'Projet d’analyse et de conception', '1450'),
       (29, 'Information et communication pro7fessionnelle', '1754'),
       (30, 'Produits logiciels de gestion intégrés', '1438'),
       (31, 'Administration, gestion et sécurisation des réseaux', '1439'),
       (32, 'Projet de développement SGBD', '1446'),
       (33, 'Stage d’intégration professionnelle', '1451'),
       (34, 'Projet d’intégration de développement', '1447'),
       (35, 'Activités professionnelles de formation', '1452'),
       (36, 'Epreuve intégrée de la section', '1453');


-- zone de test
