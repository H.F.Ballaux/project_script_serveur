# Installation : Projet Script Serveur

## Prérequis

- WampServer ou equivalent
- PHP version 8.2
- MySQL version 5.6 ou supérieure.
- Git project: https://gitlab.com/H.F.Ballaux/project_script_serveur.git

## Installation via Git Repository:

Dans votre dossier 'www' de Wamp64 (ou equivalent selon le serveur utilisé), vous allez y cloner le projet git, via la
commande suivante, dans le terminal du system d'exploitation ou dans Git bash

```
git clone https://gitlab.com/H.F.Ballaux/project_script_serveur.git
```

Accedez ensuite au dossier 'sql', dans lequel vous trouverez le fichier 'web_ex.sql' contenant la Base de Données du
site, il vous faudra l'importer dans MySQL via phpMyadmin

Pour ce faire dans phpMyadmin vous allez créer une nouvelle base de données,
retourner dans le menu et via l'onglet 'importer' vous aurez la possibilitée de sélectionner un fichier vous y mettrez
le fichier 'web_ex.sql'

Il faudra ensuite configurer le fichier 'config.php' pour ce faire, faites une copie du fichier
'config-dist.php', renommez le en 'config.php' et ouvrez-le dans un éditeur de texte.

Dans ce fichier, il faudra modifier les champs suivants :

- DB_NAME : Le nom de la base de données  (celle créée dans phpMyadmin)
- DB_USER : Le nom d'utilisateur de la base de données
- DB_PASSWORD : Le mots-de-passes d'utilisateur de la base de données
- DB_HOST : Address d'hébergement de la base de données

Il ne vous reste plus qu'à enregistrer les modifications apportées au fichier et démarrer votre nouveau site a l'address
suivante :

- http://localhost/project_script_serveur

Remplacez 'localhost' par address d'hébergement de la base de données référencée dans le fichier 'config.php'

## Name

Project Script Server - Hector Fabio Ballaux

## Description

Examen Script-Serveur BES WEB 1

## Author

Hector Fabio Ballaux

## Project status

exam release