<?php

$msg = ' -- Inscription -- <br>';
$url = 'index.php?vue=vue/';

if (!empty($_POST['username']) && !empty($_POST['pwd']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

    foreach ($_POST as $key => $value) {
        $$key = $value;
    }

    if (!check_data_from_user('email', $_POST['email'])) {
        if (!check_data_from_user('username', $_POST['username'])) {
            global $connect;
            $sql = '
                insert into user (username, password, email, created, lastlogin)
                values (?, ?, ?, now(), \'2020-01-01 00:00:00\')
            ';
            $params = [trim($username), password_hash($pwd, PASSWORD_DEFAULT), $email];
            $query = $connect->prepare($sql);
            $query->execute($params);

            if ($query->rowCount()) {
                $userid = $connect->lastInsertId();
                $msg .= ucfirst($username) . ' a bien été ajouté a la DB';
                if ($userid == 1) {
                    $msg .= '<br>' . ucfirst($username) . ' es promu Admin';
                }

                if (!empty($_FILES['photo']['name'] && !empty($_FILES['photo']['tmp_name']))) {
                    if ($_FILES['photo']['type'] == 'image/jpeg' || $_FILES['photo']['type'] == 'image/jpg' || $_FILES['photo']['type'] == 'image/png') {
                        if ($_FILES['photo']['size'] <= 1024000) {
                            $image_path = ROOTH_PATH . '\\img\\profil\\';

                            if (!is_dir($image_path . $userid)) {
                                mkdir($image_path . $userid, 755);
                            }

                            $basename = $userid . '.' . explode('/', $_FILES['photo']['type'])[1];
                            $move = move_uploaded_file($_FILES['photo']['tmp_name'], $image_path . $userid . '/' . $basename);
                            $img_relPath = 'img\\profil\\' . $userid . '\\' . $basename;

                            if ($move) {
                                update_image($userid, $img_relPath);
                                $msg .= '<br> L\'image a bien été ajoutée ';
                            } else {
                                $msg .= '<br> L\'image n\'a pas été ajoutée ';
                            }
                        } else {
                            $msg .= '<br> L\'image choisie est trop volumineuse';
                        }
                    } else {
                        $msg .= '<br> Format d\'image invalide';
                    }
                } else {
                    $msg .= '<br>Aucune image spécifiée';
                }
                setAlert($msg, $url . 'login', 'success');
            } else {
                setAlert($msg . 'Something got wrong!', $url . 'inscription');
            }
        } else {
            setAlert($msg . $username . ' est déjà des nôtres!', $url . 'login', 'info');
        }
    } else {
        setAlert($msg . $email . ' est déjà des nôtres!', $url . 'login', 'info');
    }
} else {
    $msg .= 'Il manque<br>';
    if (empty($_POST['username'])) {
        $msg .= 'Le login<br>';
    }
    if (empty($_POST['pwd'])) {
        $msg .= 'Mots de passe<br>';
    }
    if (empty($_POST['email'])) {
        $msg .= 'L\'email';
    }
    setAlert($msg, $url . 'inscription', 'info');
}
