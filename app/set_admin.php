<?php

$msg = '-- Set Admin --<br>';
$url = 'index.php?vue=vue/';


if (!empty($_SESSION['userid'])){
    if (getData('user', 'id', $_SESSION['userid'])->admin){
        if (!empty($_POST['id'])){
            if (update_admin($_POST['id'], '1')){
                setAlert($msg.'Mise a jour réussie', $url.'admin', 'success');
            } else {
                setAlert($msg.'Mise a jour échouée', $url.'admin');
            }
        } else {
            setAlert($msg.'Aucun utilisateur sélectionné', $url.'admin');
        }
    } else{
        setAlert($msg.'Tu n\'as pas les droits pour faire ca' , $url.'profile');
    }
} else {
    setAlert($msg.'Tu n\'es pas des nôtres' , $url.'login','info');
}
