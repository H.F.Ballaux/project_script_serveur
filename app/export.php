<?php

session_name('ScriptServeur' . date('Y-m-d'));
session_start(['cookie_lifetime' => 3600]);

if (!empty($_SESSION['userid'])) {
    require_once '../config.php';
    require_once '../lib/pdo.php';
    require_once '../lib/output.php';
    require_once '../lib/user.php';
    $connect = connect();

    $user = getData('user', 'id', $_SESSION['userid']);
    unset($user->password);
    unset($user->image);
    if (is_a($user, 'stdClass')) {
        exportJSON($user);
    }
}
