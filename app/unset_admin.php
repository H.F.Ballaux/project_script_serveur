<?php

$msg = '-- Unset Admin --<br>';
$url = 'index.php?vue=vue/';


if (!empty($_SESSION['userid'])){
    if (getData('user', 'id', $_SESSION['userid'])->admin){
        if (!empty($_POST['id'])){
            if (check_admins()>1){
                if (update_admin($_POST['id'], '0')){
                    setAlert($msg.'Mise a jour réussie', $url.'admin', 'success');
                } else {
                    setAlert($msg.'Mise a jour échouée', $url.'admin');
                }
            } else {
                setAlert($msg.'Il faut au moins un Admin', $url.'admin');
            }
        } else {
            setAlert($msg.'Aucun utilisateur sélectionné', $url.'admin');
        }
    } else{
        setAlert($msg.'Tu n\'as pas les droits pour faire ca', $url.'profile');
    }
} else {
    setAlert($msg.'Tu n\'es pas des nôtres', $url.'login', 'info');
}
