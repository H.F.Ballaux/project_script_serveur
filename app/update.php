<?php

$msg = ' -- Update Profile -- <br>';
$url = 'index.php?vue=vue/';

if (!empty($_SESSION['userid'])) {
    if (!empty($_POST['id'])) {
        if ($_SESSION['userid'] == $_POST['id']) {
            $user = getData('user', 'id', $_POST['id']);
            $updatedDatas = [];
            $param = [];
            $url .= 'profile';

            if ($_POST['password_old'] != '' && $_POST['password'] != '') {
                if (password_verify($_POST['password_old'], $user->password)) {
                    $updatedDatas[] = 'password = ?';
                    $param[] = password_hash($_POST['password'], PASSWORD_DEFAULT);
                } else {
                    $msg .= 'Mauvais MDP, update MDP impossible<br>';
                }
            }

            if ($_POST['email'] != $user->email) {
                if (!check_data_from_user('email', $_POST['email'])) {
                    $updatedDatas[] = 'email = ?';
                    $param[] = $_POST['email'];
                } else {
                    $msg .= 'L\'email " ' . $_POST['email'] . ' " n\'est pas disponible <br>';
                }
            }

            if (count($updatedDatas) > 0) {
                global $connect;
                $sql = 'update user set ' . implode(', ', $updatedDatas) . ' where id = ?';
                $param[] = $user->id;
                $query = $connect->prepare($sql);
                $query->execute($param);
                setAlert($msg . 'Mise a jour réussie', $url, 'success');
            } else {
                setAlert($msg . 'Aucune Modification(s)!', $url, 'info');
            }
        } else {
            setAlert($msg . 'Tu ne peux pas modifier ce profil', $url . 'update');
        }
    } else {
        setAlert($msg . 'Le champs id est vide', $url . 'update', 'info');
    }
} else {
    setAlert($msg . 'Il faut se connecter', $url . 'login', 'info');
}
