<?php

$url = 'index.php?vue=vue/';
$msg = ' -- Connexion -- <br>';

if (!empty($_POST['username']) && !empty($_POST['pwd'])) {
    $username = trim($_POST['username']);
    if (check_data_from_user('username', $username)) {
        $user = getData('user', 'username', $username);
        if (password_verify($_POST['pwd'], $user->password)) {
            update_lastlogin($user->id);
            $_SESSION['userid'] = $user->id;
            setAlert($msg . 'Bienvenue ' . $user->username, $url . 'profile', 'success');
        } else {
            setAlert($msg . '!! Mauvais MDP !!', $url . 'login');
        }
    } else {
        setAlert($msg . ' !! ' . $_POST['username'] . ' n\'existe pas !! ', $url . 'inscription');
    }
} else {
    $msg .= 'Il manque :<br>';
    if (empty($_POST['username'])) {
        $msg .= ' - Login<br>';
    }
    if (empty($_POST['pwd'])) {
        $msg .= ' - Mot de Passe';
    }
    setAlert($msg, $url . 'login', 'info');
}
