<?php

const ROOTH_PATH = __DIR__;
const FILES_EXTENSION = ['.php', '.html'];
const ALERT_TYPES = ['danger', 'info', 'success', 'warning'];


// Entrez ici les informations relatives à la base de données
const DB_NAME = "";
const DB_HOST = "";
const DB_USER = "";
const DB_PASSWORD = "";
