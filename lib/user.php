<?php

/** Exporte les infos de l'utilisateur au format JSON
 * @param object $user
 * @return void
 */
function exportJSON(object $user): void
{
    $filename = $user->username . '-' . $user->id . '_' . time() . '.json';
    header('Content-type: application/json');
    header('Content-disposition: attachment; filename="' . $filename . '"');
    echo json_encode($user);
}

/** Mets à jour le champ lastLogin de la base de données
 * @param int $userid l'id de l'utilisateur
 * @return void
 */
function update_lastlogin(int $userid): void
{
    global $connect;
    $sql = 'update user set lastlogin = now() where id = ?';
    $param = [$userid];
    $query = $connect->prepare($sql);
    $query->execute($param);
}

/** Mets à jour le champ image de la base de données
 * @param int $userid
 * @param string $imgPath
 * @return void
 */
function update_image(int $userid, string $imgPath): void
{
    global $connect;
    $sql = 'update user set image = ? where id = ?';
    $param = [$imgPath, $userid];
    $query = $connect->prepare($sql);
    $query->execute($param);
}


/** Mets à jour le champ admin de la base de données
 * @param int $userid
 * @param int $status
 * @return bool
 */
function update_admin(int $userid, int $status): bool {
    global $connect;
    $sql = 'update user set admin = '.$status.' where id = ?';
    $param = [$userid];
    $query = $connect->prepare($sql);
    $query->execute($param);
    if ($query->rowCount()){
        return true;
    } else {
        return false;
    }
}