<?php


/**
 * Verifie l'existence d'un contenu et l'affiche si existant
 * @param string $page l'url de la page recherchée
 * @return void
 */
function getContent(string $page): void
{
    if (is_array(FILES_EXTENSION)) {
        $cpt = 0;
        foreach (FILES_EXTENSION as $ext) {
            $filename = $page . $ext;
            if (file_exists($filename)) {
                include $filename;
            } else {
                $cpt += 1;
            }
        }
        if ($cpt == 2) {
            include 'vue/not_found.php';
        }
    }
}


/** Recupere une donnée dans la DB
 * @param string $table table de la DB
 * @param string $field champ de la table
 * @param string $value ligne recherchée
 * @return false|object retourne un objet du resultat de la recheche, ou false si la donnée n'existe pas
 */
function getData(string $table, string $field, string $value): object|bool
{
    if (in_array($table, getTables())) {
        if (in_array($field, getFields($table))) {
            global $connect;
            $sql = 'select * from ' . $table . ' where ' . $field . ' = ?';
            $param = [$value];
            $query = $connect->prepare($sql);
            $query->execute($param);
            return $query->fetchObject();
        }
    }
    return false;
}


/** Retourne une liste des éléments d'une table ciblée
 * @param string $table nom de la table ciblée
 * @return array
 */
function getList(string $table): array
{
    $result = [];
    if (in_array($table, getTables())) {
        global $connect;
        $sql = 'select * from ' . $table;
        $query = $connect->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
    }
    return $result;
}


/** Retourne le nombre d'admins présent dans la db
 * @return int
 */
function check_admins(): int
{
    $users = getList('user');
    $count_admins = 0;
    foreach ($users as $user) {
        if ($user['admin']) {
            $count_admins += 1;
        }
    }
    return $count_admins;
}


/** Transforme le format de la date vers un format européen
 * @param string $valueDate
 * @return string
 * @throws Exception
 */
function transform_date(string $valueDate): string
{
    return date_format(new DateTime($valueDate), 'd/m/y H\hi');
}


/** Parametre les alertes a afficher
 * @param string $msg le message qui sera affiché en alert
 * @param string $url l'url de la page ou l'utilisateur sera redirigé
 * @param string $type le type d'alert influe sur la couleur
 * @return void
 */
function setAlert(string $msg, string $url, string $type = 'danger'): void
{
    $_SESSION['alert'] = $msg;
    $_SESSION['alert-color'] = $type;
    header('location:' . $url);
    die;
}


/** Affiche le message d'alerte
 * @param string $color Couleur de l'alerte
 * @param string $msg message a afficher
 * @return void
 */
function showAlert(string $color, string $msg): void
{
    echo '<div class="alert-' . $color . '">' . $msg . '</div>';
}
