<?php

/** Définit la connexion avec la DB
 * @return PDO|void|null
 */
function connect()
{
    global $connect;
    if (!is_a($connect, "PDO")) {
        try {
            $connect = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8', DB_USER, DB_PASSWORD);
        } catch (PDOException $e) {
            die ('Erreur: ' . $e->getMessage());
        }
    }
    return $connect;
}

/** Retourne les tables présente dans la DB
 * @return array
 */
function getTables(): array
{
    global $connect;
    $sql = 'show tables';
    $query = $connect->prepare($sql);
    $query->execute();
    $result = $query->fetchAll();
    $retour = [];
    foreach ($result as $item) {
        $retour[] = $item[0];
    }
    return $retour;
}

/** Retourne les champs présents dans la table ciblée
 * @param string $table table ciblée
 * @return array|bool
 */
function getFields(string $table): array|bool
{
    if (in_array($table, getTables())) {
        global $connect;
        $sql = 'describe ' . $table;
        $query = $connect->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        $tab = [];
        foreach ($result as $item) {
            $tab[] = $item['Field'];
        }
        return $tab;
    }
    return false;
}

/** Verifie si une valeur existe déja dans la table user
 * @param string $field champ ciblé
 * @param string $value valeur recherchée
 * @return bool
 */
function check_data_from_user(string $field, string $value): bool
{
    if (in_array($field, getFields('user'))) {
        $users = getList('user');
        $liste = [];
        foreach ($users as $user) {
            $liste[] = $user[$field];
        }
        if (in_array($value, $liste)) {
            return true;
        }
    }
    return false;
}