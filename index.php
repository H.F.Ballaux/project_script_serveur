<?php
// Project Script Serveur - Hector Fabio Ballaux


// Définition des sessions
session_name('ScriptServeur' . date('Y-m-d'));
session_start(['cookie_lifetime' => 3600]);


// Import des constantes et librairies
require_once 'config.php';
require_once 'lib/pdo.php';
require_once 'lib/output.php';
require_once 'lib/user.php';


// Etablissement d'une connexion à la base de données
$connect = connect();


// Contenu du Header
require_once 'structure/header.html';
echo '<h1>Script Serveur</h1> ';


// Affichage des messages d'alert
if (!empty($_SESSION['alert'])) {
    if (!empty($_SESSION['alert-color']) and in_array($_SESSION['alert-color'], ALERT_TYPES)){
        $alert_color = $_SESSION['alert-color'];
        unset($_SESSION['alert-color']);
    } else {
        $alert_color = 'danger';
    }
    showAlert($alert_color, $_SESSION['alert']);
    unset($_SESSION['alert']);
}
if (!empty($_GET['logout'])) {
    showAlert('success', 'Au revoir');
}


// Contenu de la navbar
require_once 'structure/toolbar.html';
require_once 'vue/menu.php';


// Contenu du Conteneur principal
require_once 'structure/content.html';
if (!empty($_GET['vue'])) {
    getContent($_GET['vue']);
} else {
    getContent('vue/accueil');
}


// Contenu du Footer
require_once 'structure/footer_up.html';
echo '<em>Project Script Server 2023</em>';
require_once 'structure/footer.html';
